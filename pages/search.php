<?php
$searchCommentsCount="";
$search = $_POST["search"] ?? $_POST["search"];
$data = Db::selectAll("select * from comments where body like '%$search%' order by id asc");
$searchCommentsCount = Db::count("SELECT COUNT(*) as n from comments WHERE body like '%$search%'");

//echo "<pre>";
//print_r($data);
?>
<section class="row">


    <article class="col-md-12">

        </div>
        <div class="panel panel-primary">

            <div class="panel-heading">
                <form action="" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Поиск по комментариям</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                               placeholder="Поиск работает  при вводе минимум 3-х символов" name="search" minlength="3" required>
                        <!--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone-->
                        <!--                            else.</small>-->
                    </div>
                    <button type="submit" class="btn btn-success"><span
                                class="glyphicon glyphicon-search"></span> Найти комментарий
                    </button>
                </form>
                <hr>
                <h3 class="panel-title"> Поиск комментариев _ <span
                            class="label label-success"><?= $searchCommentsCount['n']; ?></span>_ найдено всего</h3>

            </div>
            <?php if ($search): ?>
                <div class="panel-body" id="search_katalog">

                    <table class="table table-bordered table-striped table-hover ">

                        <tr class="success">
                            <th class="col-md-1"> id</th>
                            <th class="col-md-1"> postId</th>
                            <th class="col-md-2"> Заголовок</th>
                            <th class="col-md-2"> Email</th>
                            <th class="col-md-6"> Комментарий</th>
                            </th>
                        </tr>
                        <?php
                        foreach ($data as $i):
                            ?>
                            <tr data-toggle="modal" data-target="#video_modal_form">
                                <td class="col-md-1 id"><?= $i->id; ?></td>
                                <td class="col-md-1"><?= $i->postId; ?></td>
                                <td class="col-md-1"><?= $i->name; ?></td>
                                <td class="col-md-1"><?= $i->email; ?></td>
                                <td class="col-md-1"><?= $i->body; ?></td>
                            </tr>

                        <?php endforeach; ?>
                    </table>
                </div>
            <?php endif; ?>
            <div class="panel-footer">
            </div>
        </div>


    </article>


</section><!-- end <section class="row"> -->