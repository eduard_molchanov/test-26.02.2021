<?php
$data = Db::selectAll("select * from comments order by id desc");
$commentsCount = Db::count("SELECT COUNT(*) as n FROM comments");
//echo "<pre>";
//print_r($data);
?>
<section class="row">


    <article class="col-md-12">

        </div>
        <div class="panel panel-primary">

            <div class="panel-heading">
                <!--                <form action="/core/router.php" method="post">-->
                <!--                    <button type="submit" class="btn btn-success" name="add_video"><span-->
                <!--                                class="glyphicon glyphicon-film"></span> Добавить комментарий-->
                <!--                    </button>-->
                <!--                </form>-->
                <hr>
                <h3 class="panel-title"> Каталог комментариев ______ <span
                            class="label label-success"><?= $commentsCount['n']; ?></span>______ всего</h3>

            </div>
            <div class="panel-body" id="video_katalog">

                <table class="table table-bordered table-striped table-hover ">

                    <tr class="success">
                        <th class="col-md-1"> id</th>
                        <th class="col-md-1"> postId</th>
                        <th class="col-md-2"> Заголовок</th>
                        <th class="col-md-2"> Email</th>
                        <th class="col-md-6"> Комментарий</th>
                        </th>
                    </tr>
                    <?php
                    foreach ($data as $i):
                        ?>
                        <tr data-toggle="modal" data-target="#video_modal_form">
                            <td class="col-md-1 id"><?=$i->id;?></td>
                            <td class="col-md-1"><?=$i->postId;?></td>
                            <td class="col-md-1"><?=$i->name;?></td>
                            <td class="col-md-1"><?=$i->email;?></td>
                            <td class="col-md-1"><?=$i->body;?></td>
                        </tr>

                    <?php endforeach; ?>
                </table>
            </div>
            <div class="panel-footer">
            </div>
        </div>


    </article>


</section><!-- end <section class="row"> -->