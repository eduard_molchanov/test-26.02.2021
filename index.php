<?php
// error_reporting(0);

spl_autoload_register(function ($class) {

    include __DIR__ . "/core/{$class}.php";

});

include __DIR__ . "/posts.php";
include __DIR__ . "/comments.php";

$q = trim(strip_tags($_GET['q']));
$params = explode("/", $q);


include "pages/a.php";

if ($params[0] == "") {
    include "pages/posts.php";
} elseif ($params[0] == "posts") {
    include "pages/posts.php";
} elseif ($params[0] == "comments") {
    include "pages/comments.php";
} elseif ($params[0] == "search") {
    include "pages/search.php";
} else {
    echo "<h3>404 Страница не найдена</h3>";
}

include "pages/z.php";
